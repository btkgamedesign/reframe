﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpRadius : MonoBehaviour
{
    [SerializeField] PickUp pickupScript;
    bool PickingUp;
   public PlayerInput inputScript;
    private void Start()
    {
        inputScript = GetComponentInParent<PlayerInput>();
    }
    private void OnTriggerStay(Collider other)
    {
        
            if (other.gameObject.tag == "Pickup")
            {
                PickingUp = true;
                pickupScript.PickUpObject(other.transform);
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
            if(PickingUp == false)
            {
                
            }

        
    }
}
