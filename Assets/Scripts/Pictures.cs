﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pictures : MonoBehaviour
{
    public Texture2D[] SavedPictures;
    public Sprite[] CreditSprites;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {

        int Pictures = FindObjectsOfType<Pictures>().Length;
        if (Pictures != 1)
        {
            Destroy(this.gameObject);
        }
        // if more then one music player is in the scene
        //destroy ourselves
        else
        {
            DontDestroyOnLoad(gameObject);
        }

    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Level Loaded");
        Debug.Log(scene.name);
        Debug.Log(mode);
        if (SceneManager.GetActiveScene().name == "EndScene")
        {
            Debug.Log("EndScene");
            var CreditData = GameObject.Find("CreditPics").GetComponent<CreditScript>();

            for (int i = 0; i <= SavedPictures.Length; i++)
            {
                CreditSprites[i] = Sprite.Create(SavedPictures[i], new Rect(0.0f, 0.0f, SavedPictures[i].width, SavedPictures[i].height), new Vector2(0.5f, 0.5f), 100.0f);
                //CreditData.CreditImages[i].GetComponent<RawImage>().texture = SavedPictures[i];
                CreditData.CreditSprites[i].GetComponent<SpriteRenderer>().sprite = CreditSprites[i];
            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T) && SavedPictures[SceneManager.GetActiveScene().buildIndex -2] != null)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public IEnumerator TakeScreenShot()
    {

        yield return new WaitForSeconds(0.1f);
    }
}
