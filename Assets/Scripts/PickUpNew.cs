﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpNew : MonoBehaviour
{
    [SerializeField] Transform destination;
    Transform objectsTransformSaved;
    public bool isHolding = false;
    [SerializeField] float dropForce = 0f;
    public float speed = 100000.0f;

    public GameObject PickedUpItem;
    private Object objectScript;


    public void PickUpObject(Transform objectsTransform)
    {
        objectsTransform.parent = destination;
        objectScript = objectsTransform.GetComponent<Object>();
        objectsTransform.position = new Vector3(destination.position.x, destination.position.y + objectScript.OffsetY, destination.position.z );
        if(objectsTransform.tag == "NormalPivot")
        {
            objectsTransform.position = destination.position;
        }
        
        
        
        objectsTransformSaved = objectsTransform;
        isHolding = true;
    }
    public void DropObject()
    {
        if (isHolding == true)
        {
            isHolding = false;

            objectsTransformSaved.parent = null;
            Rigidbody objectRigidbody = objectsTransformSaved.gameObject.GetComponent<Rigidbody>();
            objectRigidbody.constraints = RigidbodyConstraints.None;
            objectRigidbody.AddForce(destination.forward * dropForce);
        }
    }
    public void FreezeObject()
    {
        if (isHolding == true)
        {
            isHolding = false;
            objectsTransformSaved.parent = null;
            Rigidbody objectRigidbody = objectsTransformSaved.gameObject.GetComponent<Rigidbody>();
            //objectRigidbody.constraints = RigidbodyConstraints.None;
            objectRigidbody.AddForce(destination.forward * dropForce);
        }
    }

    public void Update()
    {
        //if (isHolding == true)
        //{
        //    if(Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.LeftControl))
        //    {
        //        //transform.forward = destination.transform.forward;
        //        //objectsTransformSaved.transform.Rotate(0, -.5f, 0, Space.Self);
               
                
        //    }
        //    if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftControl))
        //    {
        //        objectsTransformSaved.transform.Rotate(0, .5f, 0, Space.Self);
        //    }
        //    if (Input.GetKey(KeyCode.UpArrow))
        //    {
        //        objectsTransformSaved.transform.Rotate(-0.5f, 0, 0, Space.Self);
        //    }
        //    if (Input.GetKey(KeyCode.DownArrow))
        //    {
        //        objectsTransformSaved.transform.Rotate(0.5f, 0, 0, Space.Self);
        //    }

        //    if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.LeftControl))
        //    {
        //        objectsTransformSaved.transform.Rotate(0, 0, -0.5f, Space.Self);
        //    }

        //    if(Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftControl))
        //    {
        //        objectsTransformSaved.transform.Rotate(0, 0, 0.5f, Space.Self);
        //    }

        //}
    }

   
}
