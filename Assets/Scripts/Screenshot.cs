﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Screenshot : MonoBehaviour
{
    public GameObject mainCamera;
    public GameObject screenshotCamera;
    public GameObject Pictures;
    public Texture2D ScreenShot;
    Pictures PicturesScript;
    public int SceneNum;
    public GameObject TextUI;
    // Start is called before the first frame update
    void Start()
    {
        //string folderPath = Directory.GetCurrentDirectory() + "/Screenshots/";
        //mainCamera = GameObject.Find("Main Camera");
        TextUI = GameObject.Find("Canvas");
        screenshotCamera.SetActive(false);
        Pictures = GameObject.Find("Pictures");
        PicturesScript = Pictures.GetComponent<Pictures>();
        SceneNum = SceneManager.GetActiveScene().buildIndex;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        if(Input.GetKeyDown(KeyCode.C))
        {
            // TakeLocalScreenshot();
            StartCoroutine(PictureTimer());
        }
    }
    

    void takeLocalScreenShot()
    {
        ScreenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ScreenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ScreenShot.Apply();
        //PicturesScript.SavedPictures[SceneNum] = ScreenShot;
    }

    public IEnumerator PictureTimer()
    {
        //TextUI.SetActive(false);
        mainCamera.SetActive(false);
        screenshotCamera.SetActive(true);
        yield return new WaitForEndOfFrame();
        takeLocalScreenShot();
        PicturesScript.SavedPictures[SceneNum-2] = ScreenShot;
        mainCamera.SetActive(true);
        screenshotCamera.SetActive(false);
       // TextUI.SetActive(true);
    }



}
