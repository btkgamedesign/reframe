﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    public Material NotPickedUpMaterial;
    public Material PickedUpMaterial;
    public float speed = 10.0f;

    public float OffsetY;
    
    Rigidbody m_Rigidbody;
    public Vector3 m_EulerAngleVelocity;
    // Start is called before the first frame update
    void Start()
    {
        PickedUpMaterial = GetComponent<MeshRenderer>().material;
        GetComponent<MeshRenderer>().material = NotPickedUpMaterial;
        m_Rigidbody = GetComponent<Rigidbody>();
        m_EulerAngleVelocity = new Vector3(0, 100, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Rotate()
    {
        Quaternion deltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.deltaTime);
        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * deltaRotation);
    }
}
