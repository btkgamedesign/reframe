﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Screenshot PictureCameraScript;
    public GameObject CanvasObj;
    public PlayerInput inputScript;
    // Start is called before the first frame update
    void Start()
    {
        CanvasObj = GameObject.Find("Canvas");
        CanvasObj.SetActive(false);
        PictureCameraScript = GameObject.Find("Camera").GetComponent<Screenshot>();
        Cursor.visible = false;
        Screen.lockCursor = true;
        inputScript = GameObject.Find("player").GetComponent<PlayerInput>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            StartCoroutine(PictureCameraScript.PictureTimer());
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(CanvasObj.active)
            {
                Debug.Log("Menu");
                CanvasObj.SetActive(false);
                Screen.lockCursor = true;
                inputScript.speed = inputScript.originSpeed;
            }
            else
            {
                CanvasObj.SetActive(true);
                Screen.lockCursor = false;
                inputScript.speed = 0;
            }
           
        }
    }
    public void LoadMenu()
    {
        SceneManager.LoadScene("Title");
    }
}
